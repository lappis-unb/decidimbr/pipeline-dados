# ------- 
#
# Codigo para a geracao das chaves de seguranca para o airflow.
#
# -------

from cryptography.fernet import Fernet

fernet_key = Fernet.generate_key()
ssecret_key = Fernet.generate_key()

print(f"Fernet_key: {fernet_key.decode()}")
print(f"Secret_key: {ssecret_key.decode()}")
