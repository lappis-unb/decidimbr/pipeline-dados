# Pipeline Dados

Este repositório foi adaptado a partir da solução do Ministério da Gestão e Inovação em Serviços Públicos do Brasil para o Airflow disponível em https://github.com/gestaogovbr/airflow2-docker.


## Datahub

https://datahubproject.io/docs/quickstart/

python3 -m pip install --upgrade pip wheel setuptools
python3 -m pip install --upgrade acryl-datahub
datahub version
datahub docker quickstart


OBS

Make sure to allocate enough hardware resources for Docker engine. Tested & confirmed config: 2 CPUs, 8GB RAM, 2GB Swap area, and 10GB disk space.