env: env/touchfile

env/touchfile: requirements.txt
    test -d env || virtualenv env
    . env/bin/activate; pip install -Ur requirements.txt
    touch env/touchfile

# test: env
#     . env/bin/activate; nosetests project/test

clean:
    rm -rf env
    find -iname "*.pyc" -delete